﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DM.MovieApi;
using DM.MovieApi.ApiResponse;
using DM.MovieApi.MovieDb.Genres;
using DM.MovieApi.MovieDb.Movies;

namespace MovieSearch
{
    public class MovieHelper
    {
        public IApiMovieRequest _database;

        public MovieHelper()
        {
            //Connect to the movie database
            MovieDbFactory.RegisterSettings(new MyDbSettings());
            _database = MovieDbFactory.Create<IApiMovieRequest>().Value;
        }

        public IApiMovieRequest getDatabase()
        {
			_database = MovieDbFactory.Create<IApiMovieRequest>().Value;
            return _database;
        }

		public Uri getImagePath(string path)
		{
			return new Uri("http://image.tmdb.org/t/p/w342" + path);
		}

		public string getGenre(IReadOnlyList<Genre> genre)
		{
			string movieGenre = "";
			if (genre != null)
			{
				int iter = genre.Count;
				foreach (var g in genre)
				{
					movieGenre += g.Name;
					if (iter != 1)
					{
						movieGenre += ", ";
					}
					iter--;
				}
			}
			return movieGenre;
		}

		public string getCastMembers(ApiQueryResponse<MovieCredit> response)
		{
			var actors = "";
			if (response.Item != null && response.Item.CastMembers != null)
			{
				var castMembers = response.Item.CastMembers;
				if (castMembers != null)
				{
					//Walk through the list and take the first 3 actors listed as long as the castmember exists (!null). 
					int count = 0;
					foreach (MovieCastMember member in castMembers)
					{
						if (member != null && count != 3)
						{
							actors += member.Name;
							count++;
						}
						if (count != 3)
						{
							actors += ", ";
						}
						else
						{
							break;
						}
					}
				}

			}
			return actors;
		}

    }
}
