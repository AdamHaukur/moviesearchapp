﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DM.MovieApi.ApiResponse;
using Xamarin.Forms;
using DM.MovieApi.MovieDb.Movies;
using System.IO;

namespace MovieSearch
{
    class MainPage : ContentPage
    {
        private IApiMovieRequest _database;
        private List<Movie> _movieList;
		private MovieHelper _helper;
		private List<Movie> _movie;

		//Can throw out database, we get it through helper.getDatabase()
		public MainPage(MovieHelper helper, IApiMovieRequest database, List<Movie> movieList)
        { 
			this._database = database;
            this._movieList = movieList;
			this._helper = helper;
			this._movie = new List<Movie>();

            this.BackgroundColor = Color.FromRgb(153, 0, 0);

            this.Content = new StackLayout
            {
                Margin = 30,
                VerticalOptions = LayoutOptions.Start,
                Spacing = 10,
                Children =
                {
                    new StackLayout {Children = {this._entryLabel, this._moviEntry, },},
                    this._searchMovieButton,
                    this._displayLabel,
                    this._spinner,
                }
            };
            
			//Listens for if the button is clicked, if so it gets the results from database
            this._searchMovieButton.Clicked += async (sender, args) =>
            {
                if (_moviEntry.Text != null)
                {
                    this._searchMovieButton.IsVisible = false;
                    this._spinner.IsVisible = true;

                    ApiSearchResponse<MovieInfo> response = await _database.SearchByTitleAsync(_moviEntry.Text);

                    //List of local paths of the movie posters
                    this._movie.Clear();

                    foreach (MovieInfo res in response.Results)
                    {
                        Movie m = new Movie();

                        m.Title = res.Title;
                        m.Year = res.ReleaseDate.Year.ToString();
                        m.OverView = res.Overview;
                        m.imagePath = _helper.getImagePath(res.PosterPath);
                        m.genre = _helper.getGenre(res.Genres);
                        var credits = await _database.GetCreditsAsync(res.Id, "en");
                        m.Actors = _helper.getCastMembers(credits);

                        this._movie.Add(m);
                    }
                    if (!this._movie.Count.Equals(0))
                    {
                        await this.Navigation.PushAsync(new MovieListPage() {BindingContext = this._movie});
                    }
					//No results found that match the text entered
                    else
                    {
                        string MovieNameErrorLabel = "No movie found with the title " + _moviEntry.Text;
                        await DisplayAlert("Not Found", MovieNameErrorLabel, "OK");
                    }

                    this._moviEntry.Text = string.Empty;
                    this._spinner.IsVisible = false;
                    this._searchMovieButton.IsVisible = true;
                }
				//Trying to search for nothing
                else
                {
                    await DisplayAlert("Invalid input", "Please enter a movie", "OK");
                }
                
            };
        }

        private Label _entryLabel = new Label
        {
            HorizontalOptions = LayoutOptions.Center,
            Text = "Enter a title of a movie: ",
            TextColor = Color.White,
            FontAttributes = FontAttributes.Bold,
            FontSize = 20,
        };

        private Entry _moviEntry = new Entry
        {
            Placeholder = "Title or part of title",
            HorizontalOptions = LayoutOptions.Fill,
        };

        private Button _searchMovieButton = new Button
        {
            Text = "Search movie",
            TextColor = Color.FromRgb(64,64,64),
            BackgroundColor = Color.FromRgb(255,153,153),
            HorizontalOptions = LayoutOptions.Fill,
        };

        private Label _displayLabel = new Label
        {
            Text = string.Empty,
            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
        };

        private ActivityIndicator _spinner = new ActivityIndicator
        {
            IsRunning = true,
            IsVisible = false,
            Color = Color.Yellow,
        };
    }
}
