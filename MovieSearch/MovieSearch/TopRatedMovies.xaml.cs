﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DM.MovieApi.ApiResponse;
using DM.MovieApi.MovieDb.Movies;
using Xamarin.Forms;

namespace MovieSearch
{
    public partial class TopRatedMovies : ContentPage
    {
        private IApiMovieRequest _database;
		private ApiSearchResponse<MovieInfo> _response;
		private MovieHelper _helper;
		private ApiQueryResponse<MovieCredit> _credit;
		private List<Movie> _movie;

		public TopRatedMovies()
		{
			this.InitializeComponent();
			//theSpinner.IsVisible = false;
		}
		//Can throw out database, we get it through helper.getDatabase()
		public TopRatedMovies(MovieHelper helper, IApiMovieRequest database)
        {
            InitializeComponent();
			this._database = database;
			this._helper = helper;
			this._movie = new List<Movie>();

        }
		//Go through the results to show them on the toprated page
		public async Task getTopList(ApiSearchResponse<MovieInfo> topList)
		{
			if (!listview.IsRefreshing)
			{
				Spinner.IsRunning = true;
				Spinner.IsVisible = true;
			}
            _response = topList;

			this._movie.Clear();
			if (_response != null && _response.Results != null)
			{
				foreach (MovieInfo res in _response.Results)
				{
					Movie m = new Movie();

					m.Title = res.Title;
					m.Year = res.ReleaseDate.Year.ToString();
					m.OverView = res.Overview;
					m.imagePath = this._helper.getImagePath(res.PosterPath);
					m.genre = this._helper.getGenre(res.Genres);
					await getCredits(res);
					m.Actors = _helper.getCastMembers(_credit);

					this._movie.Add(m);
				}
			}
			BindingContext = this._movie;

			if (!listview.IsRefreshing)
			{
				Spinner.IsRunning = false;
				Spinner.IsVisible = false;
			}
        }

		public async Task getCredits(MovieInfo response)
		{
			var credit = await _database.GetCreditsAsync(response.Id, "en");
			this._credit = credit;
		}

		public async Task getTopRated()
		{
            _response = await _database.GetTopRatedAsync(1, "en");
        }

		public async Task getCredits(int response)
		{
			_credit = await _database.GetCreditsAsync(response, "en");

		}
        private async void Listview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }
            Movie movie = (Movie)e.SelectedItem;
            await this.Navigation.PushAsync(new MovieInfoPage() { BindingContext = movie });
            ((ListView)sender).SelectedItem = null;
        }

		//Refresh page, IsRefreshing is set to false while page is reloading the spinner handles the "notification" of still loading
		//Why? Because it is so pretty
		async void Handle_Refreshing(object sender, System.EventArgs e)
		{
			var TopRatedlist = await _database.GetTopRatedAsync();
			await getTopList(TopRatedlist);
			listview.IsRefreshing = false;
		}
    }
}
