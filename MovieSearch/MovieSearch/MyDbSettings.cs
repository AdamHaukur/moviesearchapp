﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DM.MovieApi;

namespace MovieSearch
{
    public class MyDbSettings : IMovieDbSettings
    {
        public MyDbSettings()
        {
        }

        public string ApiKey
        {
            get
            {
                //Put API token here. Get the token by sign up at https://www.themoviedb.org/
                return "";
            }
        }

        public string ApiUrl
        {
            get
            {
                return "http://api.themoviedb.org/3/";
            }
        }
    }
}
