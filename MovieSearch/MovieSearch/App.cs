﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DM.MovieApi.MovieDb.Movies;
//using Javax.Security.Auth;
using Xamarin.Forms;

namespace MovieSearch
{
    public class App : Application
    {
		public MovieHelper helper;

		private IApiMovieRequest _database;
		private TopRatedMovies TopRatedPage;
		private PopularMovies PopularPage;
        public App()
        {

            //on platform check it !!!

            //Connect to the movie database
            helper = new MovieHelper();
            _database = helper.getDatabase();
            

            // The root page of your application
            var mainPage = new MainPage(helper, _database, new List<Movie>())
            {
                Title = "Movie Search",
            };
            var MainNavigationPage = new NavigationPage(mainPage)
            {
                Title = "Search",
            };
            MainNavigationPage.Icon = "searchicon";

            TopRatedPage = new TopRatedMovies(helper, _database)
            {
                Title = "Top Rated Movies",
            };
            var TopRatedNavigationPage = new NavigationPage(TopRatedPage)
            {
                Title = "Top",
            };
            TopRatedNavigationPage.Icon = "favorites";

            PopularPage = new PopularMovies(helper, _database)
            {
                Title = "Popular Movies",
            };
            var PopularNavigationPage = new NavigationPage(PopularPage)
            {
                Title = "Popular",
            };
            PopularNavigationPage.Icon = "mostviewed";

            var tabbedPage = new TabbedPage();
            tabbedPage.Children.Add(MainNavigationPage);
            tabbedPage.Children.Add(TopRatedNavigationPage);
            tabbedPage.Children.Add(PopularNavigationPage);

            this.MainPage = tabbedPage;
        }

        protected override async void OnStart()
        {
            // Handle when your app starts
			var TopRatedlist = await _database.GetTopRatedAsync();
			TopRatedPage.getTopList(TopRatedlist);
			var Popularlist = await _database.GetPopularAsync();
			PopularPage.getPopList(Popularlist);

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


    }
}
