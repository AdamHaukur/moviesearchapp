﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MovieSearch
{
    public class Movie
    {
        public string Title { get; set; }

        public string Year { get; set; }

        public string genre { get; set; }

       // public string localImagePath { get; set; }

        public Uri imagePath { get; set; }

        public ImageSource ImageResource 
                => ImageSource.FromUri( this.imagePath);

        public string Actors { get; set; }

        public string OverView { get; set; }

        public Movie()
        {
            Title = "";
            Year = "";
            Actors = "";
            genre = "";
            OverView = "";
        }
    }
}
