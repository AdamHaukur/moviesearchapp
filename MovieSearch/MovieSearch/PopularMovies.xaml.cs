﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLToolkit.Forms.Controls;
using DM.MovieApi.ApiResponse;
using DM.MovieApi.MovieDb.Movies;
using Xamarin.Forms;

namespace MovieSearch
{
	public partial class PopularMovies : ContentPage
	{
		private IApiMovieRequest _database;
		private ApiSearchResponse<MovieInfo> _response;
		private MovieHelper _helper;
		private ApiQueryResponse<MovieCredit> _credit;
		private List<Movie> _movie;

		//Can throw out database, we get it through helper.getDatabase()
		public PopularMovies(MovieHelper helper, IApiMovieRequest database)
		{
			InitializeComponent();
			this._database = database;
			this._helper = helper;
			this._movie = new List<Movie>();
		}
		public async Task getPopList(ApiSearchResponse<MovieInfo> popularList)
		{
			if (!flowlistview.IsRefreshing)
			{
				Spinner.IsRunning = true;
				Spinner.IsVisible = true;
			}
            _response = popularList;

			this._movie.Clear();
			if (_response != null && _response.Results != null)
			{
				foreach (MovieInfo res in _response.Results)
				{
					Movie m = new Movie();

					m.Title = res.Title;
					m.Year = res.ReleaseDate.Year.ToString();
					m.OverView = res.Overview;
					m.imagePath = this._helper.getImagePath(res.PosterPath);
					m.genre = this._helper.getGenre(res.Genres);
					await getCredits(res);
					m.Actors = _helper.getCastMembers(_credit);
					this._movie.Add(m);
				}
			}
			BindingContext = this._movie;

			if (!flowlistview.IsRefreshing)
			{
				Spinner.IsRunning = false;
				Spinner.IsVisible = false;
			}
        }

		public async Task getCredits(MovieInfo response)
		{
			var credit = await _database.GetCreditsAsync(response.Id, "en");
			this._credit = credit;
		}

		//Item (movie) selected, go to the Info page (MovieInfoPage)
		async void Handle_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			if (e.Item == null)
			{
				return;
			}
			Movie movie = (Movie)e.Item;
			await this.Navigation.PushAsync(new MovieInfoPage() { BindingContext = movie });
			((ListView)sender).SelectedItem = null;
		}

		//Refresh the page
		async void Handle_Refreshing(object sender, System.EventArgs e)
		{
			var Popularlist = await _database.GetPopularAsync();
			await getPopList(Popularlist);
			flowlistview.IsRefreshing = false;
		}
    }
}
