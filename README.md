# Movie Search App

Mobile application to search movies by title or part of the movie title.
Also the user can get Top Rated and Popular movies.

A project for Mobile development in Reykjavik University(RU), December 2017

Build with Xamarin.Forms


[App images](https://imgur.com/a/DEx3Q)

## Authors
Adam Haukur Baumruk

Gu�r�n Inga Baldursd�ttir